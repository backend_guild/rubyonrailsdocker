## Описание
Докер-контейнер содержит ruby, RubyOnRails, bundler, mysql, mongodb и phpmyadmin.
Порты и доступы описаны в docker-compose.yml

## Установка Docker
Для запуска контейнера необходимо установить docker и docker-compose. Установка под linux описана ниже
В windows для работы docker'а требуется Professional версия ОС + аппаратная поддержка виртуализации, 
что усложняет использование. Установка под windows здесь не описана. 

Установка docker под ubuntu:
```
$ sudo apt-get update
$ sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(. /etc/os-release; echo "$UBUNTU_CODENAME") stable"
$ cat /etc/apt/sources.list.d/additional-repositories.list 
$ sudo apt-get update
$ sudo apt-get -y  install docker-ce docker-compose
$ sudo usermod -aG docker $USER

```

Для проверки запустите следующие команды:
```
$ sudo docker -v
$ sudo docker-compose -v

```
Если установка прошла удачно, команды должны вывести информацию о версиях данных пакетов.

## Запуск контейнеров
Сначала необходимо скомпилировать образ:
``` 
$ sudo docker-compose build
```

После разворачивания образа можно проверить работоспособность контейнеров (в другом терминале):
```
$ sudo docker-compose run app ruby -v - покажет версию руби
$ sudo docker-compose run app bundler -v - покажет вверсию bundler 
$ sudo docker-compose exec mysql_db mysql -u root -p - после ввода пароля перейдёт в mysql
(пароль указан в docker-compose.yml)
$ sudo docker-compose exec mongo_db mongo --version - покажет версию mongo
```

Запуск контейнера:
```
$ sudo docker-compose up
```

При первом запуске создаём базу данных:
```
$ sudo docker-compose exec app rails db:create
```


Остановка контейнера:
``` 
$ sudo docker-compose down
```

## Особенности
Т.к. установлены две базы - и mysql, и mongodb, чтобы создать модель под mysql необходимо добавить опцию:
```
$ sudo docker-compose exec app rails generate model User --orm=mysql2
```
(не реализовано на данный момент, т.к. всё ещё есть проблемы с mongo)

## Адреса
localhost:8083 - сайт на Ruby on Rails. Запущен в обход nginx через команду rails s (встроенный сервер). Должна отображаться страница "Yay! You’re on Rails!"
localhost:8081 - страница, настроенная на nginx. Отображаеся Welcome to nginx
localhost:8767 - phpmyadmin
Все порты могут отличаться от указанных здесь, лучше смотреть актуальные данные в docker-compose.yml

## Проблемы в данной сборке
- nginx не подтягивает настройки из my_app.conf - сервер работает в обход nginx. В nginx не удалось вывести даже кастомный index.html
- в mongo не удаётся даже просмотреть базу командой show dbs, для просмотра недостаточно прав (unauthorized)
Как проверить: 
``` 
$ sudo docker-compose exec mongo_db mongo
$ show dbs 
```
- Ruby on Rails не подключён к mongo

## Конфиги
nginx - файл my_app.conf 
создание пользователя mongodb - init-mongo.js (не работает)
Dockerfile, docker-compose.yml - настройки докера
docker-entrypoint.sh - выполнение команд перед запуском контейнеров (сейчас не используется)
config/database.yml - настройки подключения Ruby on Rails к БД