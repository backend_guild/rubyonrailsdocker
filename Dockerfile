FROM ruby:2.7
MAINTAINER maintainer@example.com

WORKDIR /var/www/
COPY . /var/www/
RUN gem install bundler -v 2.0.2 --force
RUN bundle install


# allows a shell script to run before any relative containers execute a command.
#COPY ./docker-entrypoint.sh /
#RUN chmod +x /docker-entrypoint.sh
#ENTRYPOINT ["/docker-entrypoint.sh"]